<?php

require(__DIR__.'/../libraries/Rest_Controller.php');
defined('BASEPATH') or exit('Bad request');
class Api extends Rest_Controller{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');

	}

	public function list_get()			// - GET list
	{
		$r = $this->user_model->read();
		$this->response($r);
	}

	public function data_put(){		    // - PUT data
		$data = array('email' => $this->input->get('email'),
			'name' => $this->input->get('name')


		);
		$r = $this->user_model->insert($data);
		$this->response($r);
	}

	public function item_get(){			// GET item
		$custID=$this->get('id');
		$r=$this->user_model->readID($custID);
		$this->response($r);


	}
}
